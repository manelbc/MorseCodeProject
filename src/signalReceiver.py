import serial


def serial_data(port, baudrate):
    ser = serial.Serial(port, baudrate)
    while True:
        yield ser.read(150)

for line in serial_data('/dev/ttyACM0', 9600):
    print(line.decode('UTF-8)'))
