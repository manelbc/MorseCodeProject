#define signalButtonPin 5
#define ledPin 6
#define piezoPin 7
#define EOTButtonPin 8

bool serialPortStarted = false;

void setup() 
{
    pinMode(signalButtonPin, INPUT);
    pinMode(ledPin, OUTPUT);
    pinMode(piezoPin, OUTPUT);
    pinMode(EOTButtonPin, INPUT);
}

void loop()
{
    if (isButtonPressedFirstTime())
    {
        startTransmission();
    }
    else if (awaitingUserInput())
    {
        return;
    }
    else if (isEOTButtonPressed())
    {
        endTransmission();
    }
    
    
    if (isSignalButtonPressed())
    {
        sendSignalON();
    }
    else
    {        
        sendSignalOFF();
    }
}

bool awaitingUserInput()
{
    return !isSignalButtonPressed() && serialPortStarted == false;
}

bool isButtonPressedFirstTime()
{
    return isSignalButtonPressed() && serialPortStarted == false;
}

bool isSignalButtonPressed()
{
    return digitalRead(signalButtonPin) == HIGH;
}

bool isEOTButtonPressed()
{
    return digitalRead(EOTButtonPin) == HIGH;
}

void startTransmission()
{
    Serial.begin(9600);
    delay(100); // Might take a bit to open the serial port
    serialPortStarted = true;
}

void endTransmission()
{
    Serial.print(" ");
    Serial.end();
    serialPortStarted = false;
}

void sendSignalON()
{
    Serial.print("1");
    tone(piezoPin, 500);
    digitalWrite(ledPin, HIGH);
}

void sendSignalOFF()
{
    Serial.print("0");
    noTone(piezoPin);
    digitalWrite(ledPin, LOW);
}